/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, {Component} from 'react';
import {StyleSheet, TouchableOpacity, Text, View, TextInput, Image,Alert,Button} from 'react-native';
import Message from './app/components/message/Message'
import Body from './app/components/body/Body'
import OurFlatList from './app/components/OurFlatList/OurFlatList'
import ConexionFetch from './app/components/conexionFetch/ConexionFetch'
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';

const provincias = [
  {
    id:1,
    name:'Arequipa',
  },
  {
    id:2,
    name:'Puno',
  },
  {
    id:3,
    name:'Cuzco',
  },
];
function HomeScreen({ navigation }) {
  return (
    <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
      <Text>Home Screen</Text>
      <Button
        title="Go to Details"
        onPress={() => navigation.navigate('Details')}
      />
    </View>
  );
}
function DetailsScreen({ navigation }) {
  return (
    <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
      <Text>Details Screen</Text>
      <Button
        title="Go back to first screen in stack"
        onPress={() => navigation.popToTop()}
      />
    </View>
  );
}
const Stack = createStackNavigator();

export default class App extends Component {
  constructor (props) {
    super(props);
    this.state ={
      textValue: '',
      count: 0,
    };
  }

  changeTextInput = text => {
    console.log(text)
    this.setState ({textValue: text});
  };
  onPress = () => {
    this.setState({
      count: this.state.count + 1,
    });
  };

  showAlert = () =>{
    Alert.alert(
      'Titulo',
      'Mensaje',
      [
        {
          text:'Cancel',
          onPress: () => console.log('Cancel Pressed'),
          style:'cancel',
        },
        {text:'OK', onPress:() => console.log('OK Pressed')},
      ],
      {cancelable: false},
    );
  };

  render() {
    return (
      <NavigationContainer>
      <Stack.Navigator>
        <Stack.Screen name="Home" component={HomeScreen} />
        <Stack.Screen name="Details" component={DetailsScreen} />
      </Stack.Navigator>
    </NavigationContainer>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    padding: 10,
  },
  text: {
    alignItems: 'center',
    padding: 10,
  },
  button: {
    top: 10,
    alignItems: 'center',
    backgroundColor: '#DDDDDD',
    padding: 10,
  },
  countContainer: {
    alignItems: 'center',
    padding: 10,
  },
  countText: {
    color: '#FF00FF',
  },
})

  /*onPress = () => {
    this.setState({
      count: this.state.count + 1,
    });
  };*/